package ru.tsc.avramenko.tm.api;

import ru.tsc.avramenko.tm.dto.AbstractEntityDTO;
import java.util.List;

public interface IRepositoryDto<E extends AbstractEntityDTO> {

    void add(final E entity);

    void update(final E entity);

    void clear();

    List<E> findAll();

}
