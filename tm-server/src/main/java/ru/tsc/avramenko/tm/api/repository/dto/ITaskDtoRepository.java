package ru.tsc.avramenko.tm.api.repository.dto;

import ru.tsc.avramenko.tm.api.IRepositoryDto;
import ru.tsc.avramenko.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskDtoRepository extends IRepositoryDto<TaskDTO> {

    TaskDTO findById(final String userId, final String id);

    TaskDTO findByName(final String userId, final String name);

    TaskDTO findByIndex(final String userId, final int index);

    void removeById(final String userId, final String id);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

    List<TaskDTO> findAllTaskByProjectId(final String userId, final String projectId);

    void unbindAllTaskByProjectId(final String userId, final String projectId);

    List<TaskDTO> findAllById(final String userId);

    void clear(final String userId);

}
