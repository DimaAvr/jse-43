package ru.tsc.avramenko.tm.api;

import ru.tsc.avramenko.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(final E entity);

    void update(final E entity);

    void clear();

    List<E> findAll();

}
