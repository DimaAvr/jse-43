package ru.tsc.avramenko.tm.api.repository.model;

import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import ru.tsc.avramenko.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task findById(final String userId, final String id);

    Task findByName(final String userId, final String name);

    Task findByIndex(final String userId, final int index);

    void removeById(final String userId, final String id);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

    List<Task> findAllTaskByProjectId(final String userId, final String projectId);

    void unbindAllTaskByProjectId(final String userId, final String projectId);

    void clear(final String userId);

    List<Task> findAllById(final String userId);

}
