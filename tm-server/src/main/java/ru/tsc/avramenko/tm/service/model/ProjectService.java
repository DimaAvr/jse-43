package ru.tsc.avramenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.model.IProjectRepository;
import ru.tsc.avramenko.tm.api.repository.model.IUserRepository;
import ru.tsc.avramenko.tm.api.service.IConnectionService;
import ru.tsc.avramenko.tm.api.service.model.IProjectService;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.*;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.system.IndexIncorrectException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.repository.model.ProjectRepository;
import ru.tsc.avramenko.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class ProjectService implements IProjectService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull
        final Project project = new Project();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final User user = repository.findById(userId);
        project.setUser(user);
        project.setName(name);
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull
        final Project project = new Project();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final User user = repository.findById(userId);
        project.setUser(user);
        project.setName(name);
        project.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            return projectRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            if (index > projectRepository.findAllById(userId).size() - 1) throw new IndexIncorrectException();
            return projectRepository.findByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @SneakyThrows
    public Project finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override@SneakyThrows
    public Project finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status);
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status);
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status);
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            return projectRepository.findAllById(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            return projectRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Project findById(@Nullable String userId, @Nullable String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            return projectRepository.findById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@Nullable List<Project> projects) {
        if (projects == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            for (@NotNull final Project project : projects) projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}