package ru.tsc.avramenko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.avramenko.tm.api.repository.model.IProjectRepository;
import ru.tsc.avramenko.tm.dto.ProjectDTO;
import ru.tsc.avramenko.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public Project findById(@Nullable final String id) {
        return entityManager.find(Project.class, id);
    }

    public void removeById(@Nullable final String id) {
        Project reference = entityManager.getReference(Project.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT e FROM ProjectDto e", Project.class).getResultList();
    }

    @Nullable
    @Override
    public Project findByName(@NotNull final String userId, @Nullable final String name) {
        List<Project> list = entityManager
                .createQuery("SELECT e FROM ProjectDto e WHERE e.name = :name AND e.user_id = :user_id", Project.class)
                .setParameter("name", name)
                .setParameter("user_id", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    @Override
    public Project findByIndex(@NotNull final String userId, final int index) {
        List<Project> list = entityManager
                .createQuery("SELECT e FROM ProjectDto e WHERE e.user_id = :user_id", Project.class)
                .setParameter("user_id", userId)
                .setFirstResult(index).setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.name = :name AND e.user_id = :user_id")
                .setParameter("user_id", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.user_id = :user_id")
                .setParameter("user_id", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM ProjectDto e")
                .executeUpdate();
    }

    @Override
    public void clear(final String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.user_id = :user_id")
                .setParameter("user_id", userId)
                .executeUpdate();
    }

    @Override
    public Project findById(final String userId, final String id) {
        List<Project> list = entityManager
                .createQuery("SELECT e FROM ProjectDto e WHERE e.id = :id AND e.user_id = :user_id", Project.class)
                .setParameter("id", id)
                .setParameter("user_id", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeById(final String userId, final String id) {
        entityManager
                .createQuery("DELETE FROM ProjectDto e WHERE e.id = :id AND e.user_id = :user_id")
                .setParameter("user_id", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public List<Project> findAllById(final String userId) {
        List<Project> list = entityManager
                .createQuery("SELECT e FROM ProjectDto e WHERE e.user_id = :user_id", Project.class)
                .setParameter("user_id", userId)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list;
    }

}