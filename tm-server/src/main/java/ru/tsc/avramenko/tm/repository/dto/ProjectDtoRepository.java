package ru.tsc.avramenko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.avramenko.tm.dto.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectDtoRepository extends AbstractDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public ProjectDTO findById(@Nullable final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public void removeById(@Nullable final String id) {
        ProjectDTO reference = entityManager.getReference(ProjectDTO.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<ProjectDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e", ProjectDTO.class).getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findByName(@NotNull final String userId, @Nullable final String name) {
        List<ProjectDTO> list = entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.name = :name AND e.userId = :userId", ProjectDTO.class)
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    @Override
    public ProjectDTO findByIndex(@NotNull final String userId, final int index) {
        List<ProjectDTO> list = entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e")
                .executeUpdate();
    }

    @Override
    public void clear(final String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public ProjectDTO findById(final String userId, final String id) {
        List<ProjectDTO> list = entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.id = :id AND e.userId = :userId", ProjectDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeById(final String userId, final String id) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.id = :id AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public List<ProjectDTO> findAllById(final String userId) {
        List<ProjectDTO> list = entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list;
    }

}