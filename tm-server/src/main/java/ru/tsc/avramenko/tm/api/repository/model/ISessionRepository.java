package ru.tsc.avramenko.tm.api.repository.model;

import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    void remove(final Session session);

    Session findById(final String id);

}
