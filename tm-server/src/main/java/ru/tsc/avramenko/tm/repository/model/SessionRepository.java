package ru.tsc.avramenko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.model.ISessionRepository;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;

import javax.persistence.EntityManager;
import javax.validation.constraints.Null;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionDto e", Session.class).getResultList();
    }

    @Override
    public void remove(Session session) {
        @Nullable final User user = session.getUser();
        Session reference = entityManager.getReference(Session.class, user.getId());
        entityManager.remove(reference);
    }

    @Override
    public Session findById(@Nullable final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDto e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        SessionDTO reference = entityManager.getReference(SessionDTO.class, id);
        entityManager.remove(reference);
    }

}