package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.dto.ProjectDTO;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.service.dto.ProjectDtoService;
import ru.tsc.avramenko.tm.service.dto.ProjectTaskDtoService;
import ru.tsc.avramenko.tm.service.dto.SessionDtoService;
import ru.tsc.avramenko.tm.service.model.ProjectService;
import ru.tsc.avramenko.tm.service.model.ProjectTaskService;
import ru.tsc.avramenko.tm.service.model.SessionService;

import java.util.List;

public class ProjectServiceTest {

    @Nullable
    private ProjectDtoService projectService;

    @Nullable
    private ProjectTaskDtoService projectTaskService;

    @Nullable
    private SessionDtoService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private ProjectDTO project;

    @Nullable
    private SessionDTO session;

    @NotNull
    protected static final String TEST_PROJECT_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "TestUserIdIncorrect";

    @NotNull
    protected static final String TEST_PROJECT_ID_INCORRECT = "647";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        projectService = new ProjectDtoService(connectionService);
        projectTaskService = new ProjectTaskDtoService(connectionService);
        sessionService = new SessionDtoService(connectionService, bootstrap);
        this.session = sessionService.open("Test", "Test");
        projectService.create(session.getUserId(), TEST_PROJECT_NAME, TEST_DESCRIPTION_NAME);
        project = projectService.findByName(session.getUserId(), TEST_PROJECT_NAME);
    }

    @After
    public void after() {
        projectService.clear(session.getUserId());
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals(TEST_PROJECT_NAME, project.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, project.getDescription());

        @Nullable final ProjectDTO projectById = projectService.findById(session.getUserId(), project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    public void findAll() {
        @NotNull final List<ProjectDTO> projects = projectService.findAll();
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<ProjectDTO> projects = projectService.findAll(session.getUserId());
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    public void findById() {
        @Nullable final ProjectDTO project = projectService.findById(session.getUserId(), this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final ProjectDTO project = projectService.findById(session.getUserId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test
    public void findByIdIncorrectUser() {
        @Nullable final ProjectDTO project = projectService.findById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void findByName() {
        @NotNull final ProjectDTO project = projectService.findByName(session.getUserId(), TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
    }

    @Test
    public void findByNameIncorrect() {
        @Nullable final ProjectDTO project = projectService.findByName(session.getUserId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @Nullable final ProjectDTO project = projectService.findByName(session.getUserId(), null);
        Assert.assertNull(project);
    }

    @Test
    public void findByNameIncorrectUser() {
        @Nullable final ProjectDTO project = projectService.findByName(TEST_USER_ID_INCORRECT, this.project.getName());
        Assert.assertNull(project);
    }

    @Test
    public void removeById() {
        projectTaskService.removeProjectById(session.getUserId(), project.getId());
        Assert.assertNull(projectService.findById(session.getUserId(), project.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        projectTaskService.removeProjectById(session.getUserId(), null);
        Assert.assertNotNull(projectService.findById(session.getUserId(), project.getId()));
    }

    @Test
    public void removeByIdIncorrect() {
        projectTaskService.removeProjectById(session.getUserId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNotNull(projectService.findById(session.getUserId(), project.getId()));
    }

    @Test
    public void removeByIdIncorrectUser() {
        projectTaskService.removeProjectById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNotNull(projectService.findById(session.getUserId(), project.getId()));
    }

    @Test
    public void startById() {
        @Nullable final ProjectDTO project = projectService.startById(session.getUserId(), this.project.getId());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = EmptyIdException.class)
    public void startByIdNull() {
        @Nullable final ProjectDTO project = projectService.startById(session.getUserId(), null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByIdIncorrect() {
        @Nullable final ProjectDTO project = projectService.startById(session.getUserId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByIdIncorrectUser() {
        @Nullable final ProjectDTO project = projectService.startById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void startByName() {
        @Nullable final ProjectDTO project = projectService.startByName(session.getUserId(), TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = EmptyNameException.class)
    public void startByNameNull() {
        @Nullable final ProjectDTO project = projectService.startByName(session.getUserId(), null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByNameIncorrect() {
        @Nullable final ProjectDTO project = projectService.startByName(session.getUserId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByNameIncorrectUser() {
        @Nullable final ProjectDTO project = projectService.startByName(TEST_USER_ID_INCORRECT, TEST_PROJECT_NAME);
        Assert.assertNull(project);
    }

    @Test
    public void startByIndex() {
        @Nullable final ProjectDTO project = projectService.startByIndex(session.getUserId(), 0);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByIndexIncorrect() {
        @Nullable final ProjectDTO project = projectService.startByIndex(session.getUserId(), 674);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByIndexIncorrectUser() {
        @Nullable final ProjectDTO project = projectService.startByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(project);
    }

    @Test
    public void finishById() {
        @Nullable final ProjectDTO project = projectService.finishById(session.getUserId(), this.project.getId());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test(expected = EmptyIdException.class)
    public void finishByIdNull() {
        @Nullable final ProjectDTO project = projectService.finishById(session.getUserId(), null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByIdIncorrect() {
        @Nullable final ProjectDTO project = projectService.finishById(session.getUserId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByIdIncorrectUser() {
        @Nullable final ProjectDTO project = projectService.finishById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void finishByName() {
        @Nullable final ProjectDTO project = projectService.finishByName(session.getUserId(), TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test(expected = EmptyNameException.class)
    public void finishByNameNull() {
        @Nullable final ProjectDTO project = projectService.finishByName(session.getUserId(), null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByNameIncorrect() {
        @Nullable final ProjectDTO project = projectService.finishByName(session.getUserId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByNameIncorrectUser() {
        @Nullable final ProjectDTO project = projectService.finishByName(TEST_USER_ID_INCORRECT, TEST_PROJECT_NAME);
        Assert.assertNull(project);
    }

    @Test
    public void finishByIndex() {
        @Nullable final ProjectDTO project = projectService.finishByIndex(session.getUserId(), 0);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByIndexIncorrect() {
        @Nullable final ProjectDTO project = projectService.finishByIndex(session.getUserId(), 674);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByIndexIncorrectUser() {
        @Nullable final ProjectDTO project = projectService.finishByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(project);
    }

    @Test
    public void changeStatusById() {
        @Nullable final ProjectDTO project = projectService.changeStatusById(session.getUserId(), this.project.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    public void changeStatusByName() {
        @Nullable final ProjectDTO project = projectService.changeStatusByName(session.getUserId(), TEST_PROJECT_NAME, Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        @Nullable final ProjectDTO project = projectService.changeStatusByIndex(session.getUserId(), 0, Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

}